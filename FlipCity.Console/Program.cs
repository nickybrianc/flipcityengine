﻿using FlipCity.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlipCity.Runner
{
    class ConsoleRunner
    {
        static void Main(string[] args)
        {
            var g = new Game(1);

            while (!g.GameOver)
            {
                var turn = g.CurrentTurn;

                while (!turn.TurnOver)
                {
                    //int optionSelector = 0;
                    foreach (var opt in turn.Choices)
                    {
                        var sb = new StringBuilder();
                        sb.AppendLine($"{opt.Prompt}");
                        foreach (var choice in opt.Choices)
                        {
                            sb.AppendLine($"\t{choice}");
                        }
                        Console.WriteLine(sb.ToString());
                    }

                    var line = Console.ReadLine().Split(' ');

                    var nums = line.Select(l => int.Parse(l)).ToArray();

                    if (nums[0] < turn.Choices.Count() && nums[1] < turn.Choices[nums[0]].Choices.Count())
                    {
                        var choice = turn.Choices[nums[0]];

                        choice.PostChoiceSelection(nums[1], turn);
                    }
                    else
                    {
                        Console.WriteLine("Try Again.");
                    }
                }
            }
        }
    }
}
