﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlipCity.Core.CardPieces
{
    class ResidentialAreaCardFace : CardFace
    {
        public ResidentialAreaCardFace(CardBase c) : base(c)
        {
            Name = "Residential Area";
            BuyCost = Constants.INVALID;
            FlipCost = 1;

            Rewards = new PlayRewards()
            {
                CoinsGained = 1,
                UnhappinessGained = 1,
            };
        }

        public override void ContinuousAction(GameTurn gt)
        {
            if (gt.CurrentPlayer.PlayableCards.Count() > 0)
            {
                var firstCard = gt.CurrentPlayer.PlayableCards.First();

                if (!firstCard.IsFlipped && typeof(ResidentialAreaCardFace) == firstCard.Front.GetType())
                {
                    gt.Choices.Clear();

                    gt.Choices.Add(
                        new Choice(
                            "You must play the Residential Area.",
                            new string[]{
                                "Okay.",
                            },
                            ResidentialAreaContinuous));
                }
            }
        }

        private void ResidentialAreaContinuous(int selection, GameTurn gti)
        {
            GameTurn.PlayCard(gti, gti.CurrentPlayer.PlayableCards.First());
        }
    }
}
