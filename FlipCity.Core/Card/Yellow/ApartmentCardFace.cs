﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlipCity.Core.CardPieces
{
    public class ApartmentCardFace : CardFace
    {
        public ApartmentCardFace(CardBase c) : base(c)
        {
            Name = "Apartment";
            BuyCost = Constants.INVALID;
            FlipCost = 8;

            Rewards = new PlayRewards()
            {
                CoinsGained = 1,
                UnhappinessGained = 1,
            };
        }

        public override void FlipAction(GameTurn gt)
        {
            gt.Choices.Clear();
            gt.Choices.Add(
                new Choice(
                    "Please select a deck to go into",
                    gt.Parent.NumPlayers > 1 ?
                        // Not a singlePlayerGame
                        gt.Parent.Players.Select(p => p.Name).ToArray() :
                        new string[]{
                        "Return to Sender"
                        },
                        ApartmentFlip));
        }

        private void ApartmentFlip(int selection, GameTurn gti)
        {
            if (gti.Parent.NumPlayers > 1)
            {
                return;
            }

            var selectedPlayer = gti.Parent.Players.FirstOrDefault(p => p.Name != gti.CurrentPlayer.Name);
            gti.CurrentPlayer.DiscardPile.Remove(this.Parent);

            selectedPlayer.DiscardPile.Add(this.Parent.Flip());
        }
    }
}
