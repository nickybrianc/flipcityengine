﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlipCity.Core.CardPieces
{
    class YellowCard : CardBase
    {
        public YellowCard(Player p) : base(p)
        {
            Front = new ResidentialAreaCardFace(this);
            Back = new ApartmentCardFace(this);
        }
    }
}
