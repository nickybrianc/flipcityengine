﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlipCity.Core.CardPieces
{
    class GreenCard : CardBase
    {
        public GreenCard(Player p) : base(p)
        {
            Front = new ConvenienceStoreFace(this);
            Back = new ShoppingMallFace(this);
        }

        internal override CardBase Clone(Player currentPlayer)
        {
            return new GreenCard(currentPlayer);
        }
    }
}
