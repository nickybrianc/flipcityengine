﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlipCity.Core.CardPieces
{
    public class ConvenienceStoreFace : CardFace
    {
        public ConvenienceStoreFace(CardBase c) : base(c)
        {
            Name = "Convenience Store";
            BuyCost = 2;
            FlipCost = 3;

            Rewards = new PlayRewards()
            {
                CoinsGained = 1,
            };
        }

        public override void PlayAction(GameTurn gt)
        {
            if (gt.Field.Count() >= 18)
            {
                gt.Parent.GameOver = true;
                gt.PlayerWon = true;
            }
        }
    }
}
