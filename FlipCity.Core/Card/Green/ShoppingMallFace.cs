﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlipCity.Core.CardPieces
{
    class ShoppingMallFace : CardFace
    {
        public ShoppingMallFace(CardBase c) : base(c)
        {
            Name = "Shopping Mall";
            BuyCost = Constants.INVALID;
            FlipCost = Constants.INVALID;

            CanRecycle = true;

            Rewards = new PlayRewards()
            {
                CoinsGained = 2,
                MedalsGained = 1,
            };
        }

        public override void ContinuousAction(GameTurn gt)
        {
            if (gt.Field.Count() > 0 && this.Parent.Id == gt.Field?.LastOrDefault()?.Id)
            {
                gt.Choices.Clear();

                gt.Choices.Add(
                    new Choice(
                        "You must play your next card.",
                        new string[]{
                            "Okay.",
                        },
                        ShoppingMallOnPlay
                    ));
            }
            
            if (gt.CurrentPlayer.DiscardPile.Contains(this.Parent))
            {
                gt.Choices.Add(
                    new Choice(
                        "Would you like to Recycle the Shopping Mall?",
                        new string[] {
                        "Yes of course"
                        },
                        ShoppingMallRecycleAction));
            }
            
        }

        private void ShoppingMallOnPlay(int selection, GameTurn gti)
        {
            GameTurn.PlayCard(gti, gti.CurrentPlayer.PlayableCards.First());
        }

        private void ShoppingMallRecycleAction(int selection, GameTurn gti)
        {
            gti.Gold += 1;
            //gti.CurrentPlayer.DiscardPile.Remove(this.Parent);
            this.Parent.Flip();
            //gti.CurrentPlayer.PlayableCards.Add(this.Parent);
        }
    }
}
