﻿using FlipCity.Core.CardPieces;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlipCity.Core.Card.Orange
{
    class FactoryFace : CardFace
    {
        public FactoryFace(CardBase cb) : base(cb)
        {
            Name = "Factory";
            BuyCost = 5;
            FlipCost = 6;

            Rewards = new PlayRewards
            {
                CoinsGained = 2,
            };
        }

        public override void PlayAction(GameTurn gt)
        {
            if(gt.CurrentPlayer.PlayableCards.Count != 0)
            {
                gt.Choices.Clear();

                gt.Choices.Add(
                    new Choice(
                        "You must play the next card due to the effect of Factory.",
                        new string[]{
                            "K."
                        },
                        FactoryPlayResolve));
            }
        }

        private void FactoryPlayResolve(int selection, GameTurn gt)
        {
            GameTurn.PlayCard(gt, gt.CurrentPlayer.PlayableCards[0]);
        }
    }
}
