﻿using FlipCity.Core.CardPieces;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlipCity.Core.Card.Orange
{
    class OrangeCard : CardBase
    {
        public OrangeCard(Player p) : base(p)
        {
            Front = new FactoryFace(this);
            Back = //new PowerPlantFace(this);
                new FactoryFace(this);
        }

        internal override CardBase Clone(Player currentPlayer)
        {
            return new OrangeCard(currentPlayer);
        }
    }
}
