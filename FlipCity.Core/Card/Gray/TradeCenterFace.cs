﻿using FlipCity.Core.CardPieces;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlipCity.Core.Card.Gray
{
    class TradeCenterFace : CardFace
    {
        public TradeCenterFace(CardBase cb) : base(cb)
        {
            Name = "Trade Center";
            CanRecycle = true;

            Rewards = new PlayRewards
            {
                CoinsGained = 2,
            };
        }

        public override void RecycleAction(GameTurn gt)
        {
            if (Parent.CurrentFace == this)
            {
                gt.CurrentUnhappiness++;

                //Reset discardpile
                gt.CurrentPlayer.PlayableCards.AddRange(gt.CurrentPlayer.DiscardPile);
                gt.CurrentPlayer.PlayableCards.SmartShuffle();
                gt.CurrentPlayer.DiscardPile.Clear();
            }
        }
    }
}
