﻿using FlipCity.Core.CardPieces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace FlipCity.Core.Card.Gray
{
    class OfficeFace : CardFace
    {
        public OfficeFace(CardBase c) : base(c)
        {
            Name = "Office";
            BuyCost = 3;
            FlipCost = 4;

            Rewards = new PlayRewards
            {
                CoinsGained = 1,
            };
        }

        public override void ContinuousAction(GameTurn gt)
        {
            if(gt.CurrentPlayer.PlayableCards.Count == 0 && Parent.CurrentFace == this)
            {
                gt.Choices.Add(
                    new Choice(
                        "Would you like to set aside this (all) office(s) in your discard pile?",
                        new string[]{
                            "Yes.",
                            "All.",
                            "No.",
                            "None."
                        },
                        ContinuousOffice));
            }
        }

        private void ContinuousOffice(int choice, GameTurn gt)
        {
            switch (choice)
            {
                //Yes
                case 0:
                    this.Parent.ShouldShuffle = true;
                    break;
                //All
                case 1:
                    gt.CurrentPlayer
                        .DiscardPile
                        .GetCardsOfType(Name)
                        .ToList()
                        .ForEach(
                            cb =>
                            {
                                cb.ShouldShuffle = true;
                            });
                    break;
                //No
                case 2:
                    Parent.ShouldShuffle = false;
                    break;
                //None
                case 3:
                    gt.CurrentPlayer
                        .DiscardPile
                        .GetCardsOfType(Name)
                        .ToList()
                        .ForEach(
                            cb =>
                            {
                                cb.ShouldShuffle = false;
                            });
                    break;
                default: throw new Exception("That is not supported by the office.");
            }
        }
    }
}
