﻿using FlipCity.Core.CardPieces;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlipCity.Core.Card.Gray
{
    class GrayCard : CardBase
    {
        public GrayCard(Player p) : base (p)
        {
            Front = new OfficeFace(this);
            Back = new TradeCenterFace(this);
        }

        internal override CardBase Clone(Player currentPlayer)
        {
            return new GrayCard(currentPlayer);
        }
    }
}
