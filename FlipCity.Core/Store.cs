﻿using System;
using System.Collections.Generic;
using System.Text;
using FlipCity.Core.CardPieces;

namespace FlipCity.Core
{
    public class StoreItem
    {
        public string Name { get; set; }
        public CardBase CardForSale { get; set; }
        public int Quantity { get; set; }
    }

    public class Store
    {
        public StoreItem[] ItemsForSale { get; set; }

        internal void Buy(GameTurn gt, int itemNo)
        {
            var item = ItemsForSale[itemNo];

            item.Quantity--;
            gt.Gold -= item.CardForSale.CurrentFace.BuyCost;

            gt.CurrentPlayer.DiscardPile.Add(item.CardForSale.Clone(gt.CurrentPlayer));
        }

        internal void Develop(int cardSelected, GameTurn gt)
        {
            var item = ItemsForSale[cardSelected];

            item.Quantity--;
            gt.Gold -= item.CardForSale.Front.BuyCost + item.CardForSale.Front.FlipCost;

            var card = item.CardForSale.Clone(gt.CurrentPlayer);

            card.Flip();

            gt.CurrentPlayer.DiscardPile.Add(card);
        }
    }
}
