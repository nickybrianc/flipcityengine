﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FlipCity.Core.CardPieces;

namespace FlipCity.Core
{
    public class GameTurn
    {
        public int Gold { get; set; }
        public int MaxUnhappiness { get; set; }
        public int CurrentUnhappiness { get; set; }
        public int Medals { get; set; }
        public int TurnCounter { get; set; }
        public bool TurnOver { get; set; }
        public bool PlayerWon { get; set; }
        public bool InActionPhase { get; set; }
        public List<Choice> ReserveChoices { get; set; }
        public List<Choice> Choices { get; set; }
        public Player CurrentPlayer { get; set; }
        public Player NextPlayer { get; set; }
        public List<CardBase> Field { get; set; }
        public GameState Parent { get; set; }

        public GameTurn(GameState g, Player p, int turnCounter)
        {
            Parent = g; CurrentPlayer = p; TurnCounter = turnCounter;

            Field = new List<CardPieces.CardBase>();

            Choices = new List<Choice>();

            ResetReserveChoices();

            ResetChoices();

            ProcessContinuous();
        }

        private void ResetChoices()
        {
            Choices.Clear();

            ReserveChoices.ForEach(choice => Choices.Add(choice));

            if (CurrentPlayer.PlayableCards.Count() > 0)
            {
                Choices.Add(
                    new Choice(
                        "Would you like to play a card?",
                        new string[]{
                            $"Yes, [{CurrentPlayer.PlayableCards.First().CurrentFace.Name}]",
                        },
                        PlayCard));
            }

            if(CurrentPlayer.PlayableCards.Count() == 0 && CurrentPlayer.DiscardPile.Count() > 0)
            {
                Choices.Add(
                    new Choice(
                        "Shuffle Discard",
                        new string[]
                        {
                            $"Every Day I'm Shuffling."
                        },
                        Shuffle));
            }

            Choices.Add(
                new Choice(
                    "End Turn.",
                    new string[]
                    {
                        "End the turn."
                    },
                    EndTurn));
            
            Choices.Add(
                new Choice(
                    "Debugger.",
                    new string[]
                    {
                        "The fuck happened."
                    },
                    Debugger));
            
        }

        private void ResetReserveChoices()
        {
            ReserveChoices =
                new List<Choice>
                {
                    new Choice(
                        "You may use an action:",
                        new string[]{
                            "Buy from store.",
                            //"Flip in discard.",
                            //"Develop from store."
                        },
                        WildAction)
                };
        }

        private static void WildAction(int choice, GameTurn gt)
        {
            switch (choice)
            {
                case 0: StoreBuy(gt); break;
                //case 1: FlipDiscard(gt); break;
                //case 2: Develop(gt); break;
                case 1:
                case 2:
                default:  throw new Exception("That's not valid as a wild action.");
            }

            gt.ReserveChoices.Remove(gt.ReserveChoices.First(c => c.Prompt == Constants.GT_PROMPT_WILD));
        }

        private static void Develop(GameTurn gt)
        {
            gt.ReserveChoices.Add(
                new Choice(
                    "These are the available cards to develop in the store:",
                    gt.Parent
                      .GeneralSupply
                      .ItemsForSale
                      .Select(
                          item =>
                            $"{item.CardForSale.Front.Name} [{item.CardForSale.Back.Name}]")
                      .ToArray(),
                    DevelopAction));

            
        }

        private static void DevelopAction(int cardSelected, GameTurn gt)
        {
            var card = gt.Parent.GeneralSupply.ItemsForSale[cardSelected];

            gt.Parent.GeneralSupply.Develop(cardSelected, gt);
        }

        private static void FlipDiscard(GameTurn gt)
        {
            gt.ReserveChoices.Add(
                new Choice(
                    "These are the available cards in your discard:",
                    gt.CurrentPlayer
                      .DiscardPile
                      .Where(card => card.CurrentFace.FlipCost != Constants.INVALID)
                      .Select(card => card.CurrentFace.Name)
                      .ToArray(),
                    FlipAction));
        }

        private static void FlipAction(int cardSelected, GameTurn gt)
        {
            var flipCard = gt.CurrentPlayer
                .DiscardPile
                .Where(card => card.CurrentFace.FlipCost != Constants.INVALID)
                .ToList()[cardSelected];
            //throw new Exception("Finish this. Needs to have a way of grabbing the card. 
            //  What if this changes? skip for now.");
            //TODO: take care of aforementioned item.
            gt.Gold -= flipCard.CurrentFace.FlipCost;

            gt.ReserveChoices.Remove(gt.ReserveChoices.First(c => c.Prompt == "These are the available cards in your discard:"));

            gt.CurrentPlayer.DiscardPile.Remove(flipCard);

            flipCard.CurrentFace.RecycleAction(gt);

            flipCard.Flip();

            gt.CurrentPlayer.DiscardPile.Add(flipCard);
        }

        private static void StoreBuy(GameTurn gt)
        {
            gt.ReserveChoices.Add(
                new Choice(
                    Constants.GT_PROMPT_STORE,
                    gt.Parent
                      .GeneralSupply
                      .ItemsForSale
                      .Select(item => item.Name)
                      .Append("Cancel.")
                      .ToArray(),
                    Purchase));
        }

        private static void Purchase(int itemNo, GameTurn gt)
        {
            gt.ReserveChoices.Remove(gt.ReserveChoices.First(c => c.Prompt == Constants.GT_PROMPT_STORE));
            //Are you canelling?
            if (itemNo == gt.Parent.GeneralSupply.ItemsForSale.Length)
            {
                gt.ResetReserveChoices();
            }
            //Nope
            else
            {
                gt.Parent.GeneralSupply.Buy(gt, itemNo);
            }
        }

        private static void PlayCard(int choice, GameTurn gt)
        {
            PlayCard(gt, gt.CurrentPlayer.PlayableCards.First());
        }

        private static void Debugger(int choice, GameTurn gt)
        {
            gt.Parent.Debugger();
        }

        private static void Shuffle(int choice, GameTurn gt)
        {
            gt.CurrentPlayer.DiscardPile.ForEach(card => gt.CurrentPlayer.PlayableCards.Add(card));
            gt.CurrentPlayer.DiscardPile.Clear();
            gt.CurrentPlayer.PlayableCards.SmartShuffle();
        }

        private static void EndTurn(int choice, GameTurn gt)
        {
            gt.TurnOver = true;
            foreach(var card in gt.Field)
            {
                gt.CurrentPlayer.DiscardPile.Add(card);
            }
        }

        public static void PlayCard(GameTurn gt, CardPieces.CardBase c)
        {
            gt.CurrentPlayer.PlayableCards.Remove(c);

            gt.Field.Add(c);

            gt.Gold += c.CurrentFace.Rewards.CoinsGained;
            gt.CurrentUnhappiness += c.CurrentFace.Rewards.UnhappinessGained;
            gt.CurrentUnhappiness -= c.CurrentFace.Rewards.HappinessGained;

            c.CurrentFace.PlayAction(gt);

            gt.ProcessContinuous();
        }

        private void ProcessContinuous()
        {
            foreach (var card in CurrentPlayer.PlayableCards)
            {
                card.CurrentFace.ContinuousAction(this);
            }

            foreach (var card in Field)
            {
                card.CurrentFace.ContinuousAction(this);
            }

            foreach (var card in CurrentPlayer.DiscardPile)
            {
                card.CurrentFace.ContinuousAction(this);
            }
        }

        public void ProcessChoice(int choiceIdx, int option)
        {
            var choice = Choices[choiceIdx];
            
            choice.PostChoiceSelection(option, this);
            ResetChoices();
            ProcessContinuous();
        }
    }

}
