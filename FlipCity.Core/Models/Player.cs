﻿using FlipCity.Core.CardPieces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlipCity.Core
{
    public class Player
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Deck PlayableCards { get; set; }
        public Deck DiscardPile { get; set; }

        public Player(int id, string Name, bool disableRandomShuffle = false)
        {
            Id = id; this.Name = Name;
            PlayableCards = 
                new Deck(disableRandomShuffle)
                {
                    new YellowCard(this),
                    new YellowCard(this),
                };

            PlayableCards.First().Flip();
            DiscardPile = new Deck(disableRandomShuffle);
        }
    }
}
