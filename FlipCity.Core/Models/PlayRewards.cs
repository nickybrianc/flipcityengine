﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlipCity.Core
{
    public class PlayRewards
    {
        public int UnhappinessGained { get; set; }
        public int CoinsGained { get; set; }
        public int HappinessGained { get; set; }
        public int MedalsGained { get; set; }
    }
}
