﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlipCity.Core.CardPieces
{
    public class CardBase
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public CardFace Front { get; set; }
        public CardFace Back { get; set; }
        public bool IsFlipped { get; set; }
        public Player Owner { get; set; }

        public bool ShouldShuffle { get; set; } = true;

        public CardFace CurrentFace
        {
            get
            {
                if (IsFlipped)
                {
                    return Back;
                }
                return Front;
            }
        }

        public CardBase(Player p)
        {
            Owner = p;
        }

        internal virtual CardBase Clone(Player currentPlayer) => new CardBase(currentPlayer);

        public CardBase Flip()
        {
            IsFlipped = !IsFlipped;
            return this;
        }

        public static bool operator==(CardBase a, CardBase b)
        {
            return a.Id == b.Id;
        }

        public static bool operator!=(CardBase a, CardBase b)
        {
            return a.Id != b.Id;
        }
    }
}
