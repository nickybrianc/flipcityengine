﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using FlipCity.Core.Card.Extensions;

namespace FlipCity.Core.CardPieces
{
    public class Deck : List<CardBase>
    {
        public bool DisableShuffle { get; set; }
        public Deck(bool disableRandomShuffle = false)
        {
            DisableShuffle = disableRandomShuffle;
        }

        public void SmartShuffle()
        {
            this.Shuffle(DisableShuffle);
        }

        public IEnumerable<CardBase> GetCardsOfType(string name)
        {
            return this.Where(cb => cb.CurrentFace.Name == name);
        }
    }
}
