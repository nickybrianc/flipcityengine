﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlipCity.Core.CardPieces
{
    public class CardFace
    {
        public string Name { get; set; }
        public int BuyCost { get; set; }
        public int FlipCost { get; set; }
        public bool CanRecycle { get; set; }

        public PlayRewards Rewards { get; set; }

        public CardBase Parent { get; set; }

        public CardFace(CardBase c)
        {
            this.Parent = c;
        }

        public virtual void PlayAction(GameTurn gt)
        {
            // DO NOTHING
        }
        public virtual void RecycleAction(GameTurn gt)
        {
            //DO NOTHING
        }
        public virtual void FlipAction(GameTurn gt)
        {
            //DO NOTHING
        }
        public virtual void BuyAction(GameTurn gt)
        {
            //DO NOTHING
        }
        public virtual void ContinuousAction(GameTurn gt)
        {
            //DO NOTHING
        }
    }
}
