﻿using FlipCity.Core.CardPieces;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlipCity.Core.Card.Extensions
{
    public static class DeckExtensions
    {
        private static Random rng = new Random();

        public static void Shuffle(this Deck deck, bool disableRandom)
        {
            if (!disableRandom)
            {
                int n = deck.Count;
                while (n > 1)
                {
                    n--;
                    int k = rng.Next(n + 1);
                    var value = deck[k];
                    deck[k] = deck[n];
                    deck[n] = value;
                }
            }
        }
    }
}
