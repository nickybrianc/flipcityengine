﻿using FlipCity.Core.Card.Gray;
using FlipCity.Core.Card.Orange;
using FlipCity.Core.CardPieces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FlipCity.Core
{
    public class GameState
    {
        public int NumPlayers { get; set; }
        public bool GameOver { get; set; }
        public List<Player> Players { get; set; }
        public GameTurn[] NextTurns { get; set; }
        public Store GeneralSupply { get; set; }

        private int _nextTurnIdx = 0;
        private GameTurn _nextTurn;

        public GameTurn CurrentTurn
        {
            get
            {
                if (NextTurns == null) return null; //not initialized yet

                if (_nextTurn == null)
                {
                    _nextTurn = NextTurns[0];
                }

                if (_nextTurn.TurnOver)
                {
                    _nextTurnIdx++;
                    NextTurns[_nextTurnIdx % NextTurns.Length] = new GameTurn(this, Players[_nextTurnIdx % NextTurns.Length], _nextTurnIdx);
                    _nextTurn = NextTurns[_nextTurnIdx % NextTurns.Length];
                }

                return _nextTurn;
            }
        }

        public GameState(int nPlayers, bool disableRandomShuffle = false)
        {
            if (nPlayers != 1) throw new ArgumentException("wrong");
            NumPlayers = nPlayers;
            Players = new List<Player>();
            for (int p = 0; p < nPlayers; p++)
            {
                Players.Add(new Player(p, $"Player {p + 1}", disableRandomShuffle));
            }

            InitializeGeneralSupply();

            NextTurns = Players.Select(p => new GameTurn(this, p, _nextTurnIdx)).ToArray();
        }

        private void InitializeGeneralSupply()
        {
            this.GeneralSupply =
                new Store()
                {
                    ItemsForSale =
                        new StoreItem[]
                        {
                            new StoreItem()
                            {
                                CardForSale = new GreenCard(null),
                                Name = "Convenience Store",
                                Quantity = 12
                            },
                            new StoreItem()
                            {
                                CardForSale = new GrayCard(null),
                                Name = "Office",
                                Quantity = 10
                            },
                            new StoreItem()
                            {
                                CardForSale = new OrangeCard(null),
                                Name = "Factory",
                                Quantity = 8
                            }
                        }
                };
        }

        public void Debugger()
        {
            System.Diagnostics.Debugger.Launch();
        }
    }
}
