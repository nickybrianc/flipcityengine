﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlipCity.Core
{
    public class Choice
    {
        public string Prompt { get; set; }
        public string[] Choices { get; set; }
        public Action<int, GameTurn> PostChoiceSelection { get; set; }

        public Choice(string question, string[] options, Action<int, GameTurn> postChoiceSelection)
        {
            Prompt = question;
            Choices = options;
            PostChoiceSelection = postChoiceSelection;
        }
    }
}
