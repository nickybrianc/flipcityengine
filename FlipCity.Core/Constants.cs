﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlipCity.Core
{
    public static class Constants
    {
        public const int INVALID = -1;

        public const string GT_PROMPT_STORE = "The store is open, please select from the following:";
        public const string GT_PROMPT_WILD = "You may use an action:";
    }
}
